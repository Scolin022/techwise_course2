describe("Week 2 Day 1 Blackjack app homework", () => {
  beforeEach(() => {
    cy.visit("/blackjack/");
  });
  it("contains the required link tag in the head", () => {
    cy.get("head>link")
      .eq(2)
      .should("have.attr", "href")
      .should("match", /about/i);
  });
  it("contains the hero section, styled as required", () => {
    cy.desktop(); // we're comparing to calculated values
    cy.get("#hero").should("have.css", "width").and("equal", "1000px");
    cy.get("#hero").should("have.css", "height").and("equal", "240px");
    cy.get("#hero").should("have.css", "float");
    cy.get("#hero").find("h1").contains("Blackjack");
  });
  it("contains the whyUS section, styled as required", () => {
    cy.desktop();
    cy.get("#whyUs").should("have.css", "width").and("equal", "400px");
    cy.get("#whyUs").should("have.css", "float");
    cy.get("#whyUs").find("p").contains("Why");
    cy.mobile();
    cy.get("#whyUs").should("have.css", "width").and("equal", "400px");
  });
  it("contains the callToAction section, styled as required", () => {
    cy.desktop();
    cy.get("#callToAction").should("have.css", "width").and("equal", "600px");
    cy.get("#callToAction").should("have.css", "float");
    cy.get("#callToAction").find("p").contains("Money");
    cy.get("#callToAction").find("button").contains("Start Playing");
    cy.mobile();
    cy.get("#callToAction").should("have.css", "width").and("equal", "400px");
  });
});
