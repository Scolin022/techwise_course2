describe("Week 2 Day 1 Blackjack app in class practice", () => {
  beforeEach(() => {
    cy.visit("/blackjack/");
  });
  it("contains the required link tags in the head", () => {
    cy.get("head>link")
      .eq(0)
      .should("have.attr", "href")
      .should("match", /reset/);
    cy.get("head>link")
      .eq(1)
      .should("have.attr", "href")
      .should("match", /common/);
  });
  it("contains the header, positioned as required", () => {
    cy.get("header").should("have.css", "position").and("equal", "fixed");
    cy.get("header").should("have.css", "top").and("equal", "0px");
  });
  it("contains the footer, positioned as required", () => {
    cy.get("footer").should("have.css", "position").and("equal", "fixed");
    cy.get("footer").should("have.css", "bottom").and("equal", "0px");
  });
  it("contains the copyright span, positioned as required", () => {
    cy.get("span#copyright")
      .should("have.css", "position")
      .and("equal", "absolute");
    cy.get("footer").should("have.css", "bottom");
    cy.get("footer").should("have.css", "right");
  });
  it("has the same background-color for both header and footer", () => {
    cy.get("header").then((header) => {
      const headerColor = header.css("background-color");
      cy.get("footer").then((footer) => {
        const footerColor = footer.css("background-color");
        expect(headerColor).equal(footerColor);
      });
    });
  });
});
