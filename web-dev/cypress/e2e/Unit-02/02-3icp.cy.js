describe("Week 2 Day 2 Blackjack app in class practice", () => {
  beforeEach(() => {
    cy.visit("/blackjack/");
  });
  it("contains the required font link", () => {
    cy.get("link")
      .invoke("attr", "href")
      .should("match", /fonts\.googleapis/);
  });
  it("uses different font familys for headings and paragraphs", () => {
    cy.get("h1")
      .should("have.css", "font-family")
      .then((x) => {
        const h1FontFamily = x;
        cy.get("p")
          .should("have.css", "font-family")
          .then((y) => {
            const pFontFamily = y;
            expect(h1FontFamily).not.equal(pFontFamily);
          });
      });
  });
  it("contains the required font sizes", () => {
    cy.get("h1").should("have.css", "font-size").should("equal", "40px");
    cy.get("#copyright")
      .should("have.css", "font-size")
      .should("equal", "10px");
  });
});
