describe("Week 2 Day 2 Blackjack app in class practice", () => {
  beforeEach(() => {
    cy.visit("/blackjack/");
  });
  it("contains the .mobileFeature element", () => {
    cy.desktop();
    cy.get(".mobileFeature").should("not.be.visible");
    cy.mobile();
    cy.get(".mobileFeature").should("be.visible");
  });
  it("contains the .disappearing element", () => {
    cy.desktop();
    cy.get(".disappearing").should("be.visible");
    cy.mobile();
    cy.get(".disappearing").should("not.be.visible");
  });
});
