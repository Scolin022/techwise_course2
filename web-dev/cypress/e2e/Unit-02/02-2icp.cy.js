describe("Week 2 Day 2 Blackjack app in class practice", () => {
  beforeEach(() => {
    cy.visit("/blackjack/");
  });
  it("contains the header, positioned as required", () => {
    cy.get("header").should("have.css", "position").and("equal", "fixed");
  });
  it("contains the footer, positioned as required", () => {
    cy.get("footer").should("have.css", "position").and("equal", "fixed");
  });
  it("contains a nav that uses flexbox", () => {
    cy.get("nav").should("have.css", "display").and("equal", "flex");
  });
});
