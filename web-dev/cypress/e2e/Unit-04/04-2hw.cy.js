describe("Week 4 Day 2 Blackjack app homework", () => {
  beforeEach(() => {
    cy.visit("/blackjack/table.html", {
      onBeforeLoad(win) {
        cy.spy(win.console, "log").as("consoleLog");
      },
    });
  });
  it("defines the appropriate setter and getter functions for the bankroll", () => {
    cy.window().then((win) => {
      const { getBankroll, setBankroll } = win;
      expect(getBankroll()).to.equal(2022);
      setBankroll(1979);
      expect(getBankroll()).to.equal(1979);
    });
  });
  it("the wagering interface functions as specified", () => {
    cy.window().then((win) => {
      const { timeToBet } = win;
      cy.contains("Hit");
      cy.contains("Stand");
      timeToBet();
      cy.get("#playersActions").should("have.css", "display", "none");
      cy.contains("Wager Amount").find("input").type("12");
      cy.contains("button", "Wager").click();
      cy.get("@consoleLog").should("be.calledWith", "12");
      cy.get("#betting").should("have.css", "display", "none");
    });
  });
  it("displays the users bankroll", () => {
    cy.window().then((win) => {
      const { timeToBet, setBankroll } = win;
      setBankroll(1979);
      timeToBet();
      cy.get("#betting").contains("$1979");
    });
  });
});
