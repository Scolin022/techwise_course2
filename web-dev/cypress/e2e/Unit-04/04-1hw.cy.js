const now = new Date();
const twentyTwoYearsAgo = new Date(now.setFullYear(now.getFullYear() - 22));
const fixture = {
  "Full Name": "Mary Potter",
  Username: "HarryPoppins",
  Password: "Supercal",
  wrongPassword: "mugglish",
  Age: 22,
  "Birth Date": twentyTwoYearsAgo.toISOString().split("T")[0],
};
describe("Week 4 Day 1 Blackjack Homework", () => {
  beforeEach(() => {
    cy.visit("/blackjack/eligibility.html", {
      onBeforeLoad(win) {
        cy.spy(win.console, "log").as("consoleLog");
      },
    });
    cy.contains("Full Name").type(fixture["Full Name"]);
    cy.contains("Username").type(fixture["Username"]);
    cy.contains("Enter Password").type(fixture["Password"]);
    cy.contains("Confirm Password").type(fixture["Password"]);
    cy.contains("Age").type(fixture["Age"]);
    cy.contains("Birth Date").type(fixture["Birth Date"]);
    cy.contains("legal").click();
    cy.contains("terms").click();
  });
  it("logs the all the `label: usersInput` lines to the console", () => {
    cy.contains("Confirm Password").clear().type(fixture["wrongPassword"]);

    cy.contains("Sign up").click();
    cy.get("@consoleLog").should(
      "be.calledWith",
      `Full Name: ${fixture["Full Name"]}`
    );
    cy.get("@consoleLog").should(
      "be.calledWith",
      `Username: ${fixture["Username"]}`
    );
    cy.get("@consoleLog").should(
      "be.calledWith",
      `Enter Password: ${fixture["Password"]}`
    );
    cy.get("@consoleLog").should(
      "be.calledWith",
      `Confirm Password: ${fixture["wrongPassword"]}`
    );
    cy.get("@consoleLog").should("be.calledWith", `Age: ${fixture["Age"]}`);
    cy.get("@consoleLog").should(
      "be.calledWith",
      `Birth Date: ${fixture["Birth Date"]}`
    );
  });
  it("logs the checkboxes appropriately", () => {
    cy.contains("Sign up").click();
    cy.get("@consoleLog").should(
      "be.calledWith",
      "The user has checked the legal checkbox"
    );
    cy.get("@consoleLog").should(
      "be.calledWith",
      "The user has checked the terms checkbox"
    );
    cy.contains("legal").click();
    cy.contains("Sign up").click();
    cy.get("@consoleLog").should(
      "be.calledWith",
      "The user has not checked the legal checkbox"
    );
    cy.get("@consoleLog").should(
      "be.calledWith",
      "The user has checked the terms checkbox"
    );
    cy.contains("terms").click();
    cy.contains("Sign up").click();
    cy.get("@consoleLog").should(
      "be.calledWith",
      "The user has not checked the legal checkbox"
    );
    cy.get("@consoleLog").should(
      "be.calledWith",
      "The user has not checked the terms checkbox"
    );
    cy.contains("legal").click();
    cy.contains("Sign up").click();
    cy.get("@consoleLog").should(
      "be.calledWith",
      "The user has checked the legal checkbox"
    );
    cy.get("@consoleLog").should(
      "be.calledWith",
      "The user has not checked the terms checkbox"
    );
  });
  it("declares the user is eligible when they pass all validations", () => {
    cy.contains("Sign up").click();
    cy.get("@consoleLog").should("be.calledWith", `The user is eligible`);
  });
  it("declares the user is ineligible when they fail password validations", () => {
    cy.contains("Confirm Password").clear().type(fixture["wrongPassword"]);
    cy.contains("Sign up").click();
    cy.get("@consoleLog").should("be.calledWith", `The user is ineligible`);
  });
  it("declares the user is ineligible when they fail age validations", () => {
    cy.contains("Age").clear().type(12);
    cy.contains("Sign up").click();
    cy.get("@consoleLog").should("be.calledWith", `The user is ineligible`);
  });
  it("declares the user is ineligible when they fail checkbox validations", () => {
    cy.contains("Confirm Password").clear().type(fixture["wrongPassword"]);
    cy.contains("Sign up").click();
    cy.get("@consoleLog").should("be.calledWith", `The user is ineligible`);
  });
  it("declares the user is ineligible when they fail username validations", () => {
    cy.contains("Username").clear();
    cy.contains("Sign up").click();
    cy.get("@consoleLog").should("be.calledWith", `The user is ineligible`);
  });
  it("declares the user is ineligible when they fail fullname validations", () => {
    cy.contains("Full Name").clear();
    cy.contains("Sign up").click();
    cy.get("@consoleLog").should("be.calledWith", `The user is ineligible`);
  });
});
