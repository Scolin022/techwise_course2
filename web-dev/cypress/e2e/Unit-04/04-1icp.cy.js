describe("Week 4 Day 1 Blackjack app in class practice", () => {
  beforeEach(() => {
    cy.visit("/blackjack/table.html", {
      onBeforeLoad(win) {
        cy.spy(win.console, "log").as("consoleLog");
      },
    });
  });
  it("the function getDeck returns a deck that includes all of my favorite cards amongst the 52", () => {
    cy.window().then((win) => {
      const deck = win.getDeck();
      const hasKnaveOfHearts = deck.some(
        (card) => card.rank == "Knave" && card.suit == "♡"
      );
      const hasAceOfClubs = deck.some(
        (card) => card.rank == "Ace" && card.suit == "♣"
      );
      expect(hasAceOfClubs).to.be.true;
      expect(hasKnaveOfHearts).to.be.true;
      expect(deck.length).to.equal(52);
    });
  });

  it("the function getRandomCard returns a card with a proper suit and rank", () => {
    const suits = ["♠", "♡", "♢", "♣"];
    const ranks = ["Ace", 2, 3, 4, 5, 6, 7, 8, 9, 10, "Knave", "Queen", "King"];
    cy.window().then((win) => {
      const randomCard = win.getRandomCard();
      const hasProperSuit = suits.includes(randomCard.suit);
      const hasProperRank = ranks.includes(randomCard.rank);
      expect(hasProperRank).to.be.true;
      expect(hasProperSuit).to.be.true;
    });
  });

  it("the Hit button logs a random card", () => {
    cy.window().then((win) => {
      cy.spy(win, "getRandomCard").as("randomCard");
      cy.contains("Hit").click();
      cy.get("@consoleLog").should("be.calledWithMatch", /did\syou\sbust\?/i);
      cy.get("@randomCard").should("be.called");
    });
  });
});
