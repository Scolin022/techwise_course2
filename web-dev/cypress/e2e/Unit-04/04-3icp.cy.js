describe("Week 4 Day 3 in class practice", () => {
  beforeEach(() => {
    cy.clearLocalStorage();
    cy.setLocalStorage("bankroll", 1011);
    cy.visit("/blackjack/table.html");
  });
  it("the setBankroll function sets local storage", () => {
    cy.window().then((win) => {
      const { setBankroll } = win;
      setBankroll(1979);
      cy.getLocalStorage("bankroll").should("equal", "1979");
    });
  });
  it("the getBankroll function uses the value in local storage", () => {
    cy.window().then((win) => {
      const { getBankroll } = win;
      console.log(getBankroll());
      expect(getBankroll()).to.equal(1011);
    });
  });
});
describe("when local storage is empty", () => {
  beforeEach(() => {
    cy.clearLocalStorage();
    cy.visit("/blackjack/table.html", {
      onBeforeLoad(win) {
        cy.spy(win.console, "log").as("consoleLog");
      },
    });
  });
  it("the getBankroll function falls back to 2022 when there is no value in local storage", () => {
    cy.window().then((win) => {
      const { getBankroll } = win;
      expect(getBankroll()).to.equal(2022);
    });
  });
});
