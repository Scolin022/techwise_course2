describe("Week 4 Day 2 Blackjack app in class practice", () => {
  beforeEach(() => {
    cy.visit("/blackjack/table.html");
  });
  it("the rankToWord function converts number ranks to words, and passes word ranks through", () => {
    cy.window().then((win) => {
      const { rankToWord } = win;
      expect(rankToWord(6)).to.equal("Six");
      expect(rankToWord(10)).to.equal("Ten");
      expect(rankToWord("Ace")).to.equal("Ace");
      expect(rankToWord("Knave")).to.equal("Knave");
    });
  });
  it("the suitToWord function converts suit symbols to words", () => {
    cy.window().then((win) => {
      const { suitToWord } = win;
      expect(suitToWord("♠")).to.equal("Spades");
      expect(suitToWord("♡")).to.equal("Hearts");
      expect(suitToWord("♢")).to.equal("Diamonds");
      expect(suitToWord("♣")).to.equal("Clubs");
      expect(suitToWord("")).to.equal("Mystery");
    });
  });
  it("the rankToValue converts ranks to data-blackjack-values", () => {
    cy.window().then((win) => {
      const { rankToValue } = win;
      expect(rankToValue("Ace")).to.equal("11/1");
      expect(rankToValue("Knave")).to.equal("10");
      expect(rankToValue(10)).to.equal("10");
      expect(rankToValue(6)).to.equal("6");
      expect(rankToValue("Face Down")).to.equal("?");
    });
  });
  it("the dealToDisplay function adds a card with the appropriate markup, text, and data-blackjack-value", () => {
    cy.window().then((win) => {
      const { dealToDisplay } = win;
      //remove all the cards
      const dealersCardList = win.document.querySelector("#dealersCards ol");
      dealersCardList.firstChild;
      while (dealersCardList.firstChild) {
        dealersCardList.removeChild(dealersCardList.firstChild);
      }
      const playersCardList = win.document.querySelector("#playersCards ol");
      playersCardList.firstChild;
      while (playersCardList.firstChild) {
        playersCardList.removeChild(playersCardList.firstChild);
      }
      dealToDisplay({
        suit: "♡",
        rank: "Knave",
      });
      cy.contains("Knave of Hearts");
      cy.get(`[data-blackjack-value="10"`);
      dealToDisplay({
        suit: "♡",
        rank: "Ace",
      });
      cy.contains("Ace of Hearts");
      cy.get(`[data-blackjack-value="11/1"`);
      dealToDisplay({
        suit: "♡",
        rank: 6,
      });
      cy.contains("Six of Hearts");
      cy.get(`[data-blackjack-value="6"`);
      dealToDisplay({
        suit: "",
        rank: "Face Down",
      });
      cy.contains("Face Down of Mystery");
      cy.get(`[data-blackjack-value="?"`);
    });
  });
  it("displays a random card on click of the hit button", () => {
    cy.window().then((win) => {
      const { dealToDisplay } = win;
      //remove all the cards
      const dealersCardList = win.document.querySelector("#dealersCards ol");
      dealersCardList.firstChild;
      while (dealersCardList.firstChild) {
        dealersCardList.removeChild(dealersCardList.firstChild);
      }
      const playersCardList = win.document.querySelector("#playersCards ol");
      playersCardList.firstChild;
      while (playersCardList.firstChild) {
        playersCardList.removeChild(playersCardList.firstChild);
      }
      cy.contains("Hit").click();
      cy.get("#playersCards li").should(($li) => {
        const text = $li.text();
        expect(text).to.match(
          /(Ace|Queen|King|Knave|Two|Three|Four|Five|Six|Seven|Eight|Nine|Ten)\sof\s(Hearts|Spades|Diamonds|Clubs)/i
        );
      });
    });
  });
});
