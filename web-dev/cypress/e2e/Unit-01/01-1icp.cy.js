describe("in class practice exercise", () => {
  it("contains the students username in the h1", () => {
    cy.visit("/");
    cy.url().then((url) => {
      const username = url.split("~")[1].slice(0, -1);
      cy.get("h1").should("contain", username);
    });
  });
  it("contains 'TechWise' in the h2 tag", () => {
    cy.visit("/");
    cy.get("h2").should("contain", "TechWise");
  });
});
