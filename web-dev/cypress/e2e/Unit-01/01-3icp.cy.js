describe("blackjack home page in class practice activity", () => {
  beforeEach(() => {
    cy.visit("/blackjack/table.html");
  });
  it("contains the required heading", () => {
    cy.get("h3").contains("Actions");
  });
  it("contains the disabled Hit button", () => {
    cy.get("section")
      .contains("Actions")
      .siblings("button[type='button']")
      .contains("Hit")
      .should("be.disabled");
  });
  it("contains the Stand button", () => {
    cy.get("section")
      .contains("Actions")
      .siblings("button[type='button']")
      .contains("Stand")
      .should("not.be.disabled");
  });
  it("contains the Split button", () => {
    cy.get("section")
      .contains("Actions")
      .siblings("button[type='button']")
      .contains("Split")
      .should("not.be.disabled");
  });
});
