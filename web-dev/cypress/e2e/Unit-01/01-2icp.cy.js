describe("blackjack home page in class practice activity", () => {
  beforeEach(() => {
    cy.visit("/blackjack/");
  });
  it("contains the appropriate items in the header nav list", () => {
    cy.get("header").find("nav").find("li").should("contain", "About");
    cy.get("header").find("nav").find("li").should("contain", "Table");
  });
  it("contains the required sections", () => {
    cy.get("section").find("h2").should("contain", "About Blackjack");
    cy.get("section").find("h2").should("contain", "Rules");
  });
  it("contains the attribution in the footer", () => {
    cy.get("footer").should("contain", "Classy Development");
  });
});
