describe("Week 3 Day 1 Blackjack app homework", () => {
  beforeEach(() => {
    cy.visit("/blackjack/table.html");
  });
  it("the Hit button has the appropriate data-user-hints", () => {
    cy.contains("Hit")
      .should("have.attr", "data-user-hints")
      .should("match", /card/i);
    cy.request("/blackjack/table.css")
      .its("body")
      .then((source) => {
        expect(source).match(
          /button:hover::(after|before)[\w\W]*content[\w\W]*attr[\w\W]*data-user-hints[\w\W]*}/i
        );
      });
  });
  it("the queen has the appropriate data-blackjack-value", () => {
    cy.contains("Queen").should("have.attr", "data-blackjack-value", "10");
    cy.request("/blackjack/table.css")
      .its("body")
      .then((source) => {
        expect(source).match(
          /ol.*li:hover::(after|before)[\w\W]*content[\w\W]*attr[\w\W]*data-blackjack-value[\w\W]*}/i
        );
      });
  });
});
