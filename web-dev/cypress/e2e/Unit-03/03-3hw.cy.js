describe("Week 3 Day 3 Blackjack app homework", () => {
  beforeEach(() => {
    cy.visit("/blackjack/eligibility.html");
  });
  it("has all the appropriately labelled material textfields", () => {
    cy.get("label")
      .contains("Age")
      .should("have.class", "mdc-floating-label")
      .siblings("input")
      .should("have.attr", "type", "number");
    cy.get("label")
      .contains(`Birth Date`)
      .should("have.class", "mdc-floating-label")
      .siblings("input")
      .should("have.attr", "type", "date");
    cy.get("label")
      .contains(`Full Name`)
      .should("have.class", "mdc-floating-label");
    cy.get("label")
      .contains(`Username`)
      .should("have.class", "mdc-floating-label");
    cy.get("label")
      .contains(`Enter Password`)
      .should("have.class", "mdc-floating-label")
      .siblings("input")
      .should("have.attr", "type", "password");
    cy.get("label")
      .contains(`Confirm Password`)
      .should("have.class", "mdc-floating-label")
      .siblings("input")
      .should("have.attr", "type", "password");
  });
  it("has both material checkboxes with proper use of for attribute on labels", () => {
    cy.get("label")
      .contains(/legal/i)
      .should("have.attr", "for", "legal-checkbox");
    cy.get("label")
      .contains(/terms\sof\sservice/i)
      .should("have.attr", "for", "terms-checkbox");
  });
  it("has the material buttons as required", () => {
    cy.contains(/sign\sup/i).should("have.class", "mdc-button--raised");
    cy.contains(/problem\sgambling/i)
      .should("have.class", "mdc-button")
      .parents("a")
      .should(
        "have.attr",
        "href",
        `https://mnapg.org/understanding-problem-gambling/`
      );
  });
});
