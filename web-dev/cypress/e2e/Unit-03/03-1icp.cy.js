describe("Week 3 Day 1 Blackjack app in class practice", () => {
  beforeEach(() => {
    cy.visit("/blackjack/");
  });
  it("contains the --primary-color custom property defined on the root class", () => {
    cy.request("/blackjack/common.css")
      .its("body")
      .then((source) => {
        expect(source).match(/--primary-color/i);
        expect(source).match(/var\(--primary-color\)/i);
      });
  });
  it("contains the anchor psuedo-classes in the right order", () => {
    cy.request("/blackjack/common.css")
      .its("body")
      .then((source) => {
        expect(source).match(
          /(a:link)[\W\w]*(a:visited)[\W\w]*(a:hover)[\W\w]*(a:focus)[\W\w]*(a:active)/m
        );
      });
  });
  it("underlines the anchor only when hovered", () => {
    cy.get("a").should("have.css", "text-decoration").should("match", /none/);
    cy.get("a")
      .eq(0)
      .realHover()
      .should("have.css", "text-decoration")
      .should("match", /underline/);
  });
});
