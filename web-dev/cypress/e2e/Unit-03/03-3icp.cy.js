describe("Week 3 Day 3 Blackjack app in class practice", () => {
  beforeEach(() => {
    cy.visit("/blackjack/table.html");
  });
  it("the #playersActions section contains the required material buttons", () => {
    cy.get("section#playersActions")
      .contains("Hit")
      .should("have.class", "mdc-button");
    cy.get("section#playersActions")
      .contains("Stand")
      .should("have.class", "mdc-button");
    cy.get("section#playersActions")
      .contains("Split")
      .should("have.class", "mdc-button");
    cy.get("section#playersActions")
      .contains("Insure")
      .should("have.class", "mdc-button");
  });
  it("the required sections are all material card components and are have span declarations in material grid", () => {
    cy.get("section#dealersCards[class^='mdc-layout-grid__cell--span']").should(
      "have.class",
      "mdc-card"
    );
    cy.get("section#playersCards[class^='mdc-layout-grid__cell--span']").should(
      "have.class",
      "mdc-card"
    );
    cy.get(
      "section#playersActions[class^='mdc-layout-grid__cell--span']"
    ).should("have.class", "mdc-card");
  });
  it("one of the dealers cards is face down", () => {
    cy.get("section#dealersCards").contains("Face Down");
  });
});
