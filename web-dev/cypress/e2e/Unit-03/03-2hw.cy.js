describe("Week 3 Day 2 Blackjack app homework", () => {
  beforeEach(() => {
    cy.visit("/blackjack/shell.html");
  });
  it("contains the 4 required css links in the proper order", () => {
    cy.get("link")
      .eq(0)
      .should("have.attr", "href")
      .should("contain", "reset.css");
    cy.get("link")
      .eq(1)
      .should("have.attr", "href")
      .should("contain", "material-components-web.min.css");
    cy.get("link")
      .eq(2)
      .should("have.attr", "href")
      .should("contain", "fonts.googleapis.com/icon?family=Material");
    cy.get("link")
      .eq(3)
      .should("have.attr", "href")
      .should("contain", "shell.css");
  });
  it("has the 2 required scripts", () => {
    cy.get("script")
      .eq(1) // starting at 1, it seems cypress prepends a script
      .should("have.attr", "src")
      .should("contain", "material-components-web.min.js");
    cy.get("script")
      .eq(2)
      .should("have.attr", "src")
      .should("contain", "shell.js");
  });
  it("has a material top app bar for the header", () => {
    cy.get("nav a")
      .eq(0)
      .should("have.attr", "href")
      .should("contain", "index.html");
    cy.get("nav a")
      .eq(1)
      .should("have.attr", "href")
      .should("contain", "rules.html");
    cy.get("nav a")
      .eq(2)
      .should("have.attr", "href")
      .should("contain", "table.html");
  });
  it("has the footer with the copyright span", () => {
    cy.get("footer span#copyright");
  });
  it("has the same background-color for both header and footer", () => {
    cy.get("header").then((header) => {
      const headerColor = header.css("background-color");
      cy.get("footer").then((footer) => {
        const footerColor = footer.css("background-color");
        expect(headerColor).equal(footerColor);
      });
    });
  });
});
