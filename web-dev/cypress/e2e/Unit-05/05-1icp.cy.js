let fetchSpy;
describe("Week 5 Day 1 Blackjack app in class practice", () => {
  beforeEach(() => {
    cy.intercept("https://www.deckofcardsapi.com/api/deck/new/shuffle*").as(
      "newDeck"
    );
    cy.visit("/blackjack/table.html", {
      onBeforeLoad(win) {
        fetchSpy = cy.spy(win, "fetch").as("fetchSpy");
      },
    });
  });
  it("the function getShoe calls its callback parameter with the deck_id returned by the api", () => {
    cy.window().then((win) => {
      const { getShoe } = win;
      const callbackSpy = cy.spy().as("callbackSpy");
      getShoe(callbackSpy);
      cy.get("@callbackSpy").should("have.been.called");
      cy.get("@callbackSpy").should(
        "have.been.calledWith",
        Cypress.sinon.match({
          remaining: 312,
          shuffled: true,
          success: true,
        })
      );
      cy.get("@callbackSpy").should(
        "have.been.calledWith",
        Cypress.sinon.match.has("deck_id")
      );
      cy.get("@fetchSpy").should(
        "have.been.calledWith",
        "https://www.deckofcardsapi.com/api/deck/new/shuffle?deck_count=6"
      );
    });
  });

  it("the function drawFourCards calls its callback parameter with the cards return from the api", () => {
    cy.wait(10000); //TODO: it is better to wait on on alias, but it's not happening today
    cy.wait("@newDeck");
    cy.window().then((win) => {
      const { deckId, drawFourCards } = win;
      const callbackSpy = cy.spy().as("callbackSpy");
      drawFourCards(callbackSpy);
      cy.get("@callbackSpy").should(
        "have.been.calledWith",
        Cypress.sinon.match.array
      );
      cy.get("@fetchSpy").should(
        "have.been.calledWithMatch",
        /https:\/\/www.deckofcardsapi.com\/api\/deck\/.*\/draw\?count=./
      );
    });
  });

  it("the function drawFourCards calls its callback parameter with the cards return from the api", () => {
    cy.wait(10000); //TODO: it is better to wait on on alias, but it's not happening today
    cy.window().then((win) => {
      const { drawFourCards } = win;
      const callbackSpy = cy.spy().as("callbackSpy");
      drawFourCards(callbackSpy);
      drawFourCards(callbackSpy);
      drawFourCards(callbackSpy);
      cy.get("@callbackSpy").should(
        "have.been.calledWith",
        Cypress.sinon.match.array
      );
      const spiedDeckId = fetchSpy.secondCall.args[0]
        .split("deck/")[1]
        .split("/draw")[0];
      console.log(spiedDeckId);
      cy.wait(5000);
      cy.request(
        `https://www.deckofcardsapi.com/api/deck/${spiedDeckId}/draw/?count=1`
      ).as("drawTheThirteenth");
      cy.get("@drawTheThirteenth").should((res) => {
        expect(res.body).to.have.property("remaining");
        expect(res.body.remaining).to.equal(299);
      });
    });
  });
});
