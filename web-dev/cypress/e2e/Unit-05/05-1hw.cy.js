describe("Week 5 Day 1 Blackjack Homework", () => {
  beforeEach(() => {
    cy.visit("/blackjack/table.html");
    cy.intercept(
      {
        method: "GET",
        url: /https:\/\/www.deckofcardsapi.com\/api\/deck\/.*\/draw\/?\?count=4/, //TODO test this url change
      },
      {
        cards: [
          {
            image: "https://deckofcardsapi.com/static/img/8D.png",
            value: "8",
            suit: "DIAMONDS",
          },
          {
            image: "https://deckofcardsapi.com/static/img/9D.png",
            value: "9",
            suit: "DIAMONDS",
          },
          {
            image: "https://deckofcardsapi.com/static/img/AC.png",
            value: "ACE",
            suit: "CLUBS",
          },
          {
            image: "https://deckofcardsapi.com/static/img/KH.png",
            value: "KING",
            suit: "HEARTS",
          },
        ],
      }
    ).as("getFourCards");
    cy.intercept(
      {
        method: "GET",
        url: /https:\/\/www.deckofcardsapi.com\/api\/deck\/.*\/draw\/?\?count=1/,
      },
      {
        cards: [
          {
            image: "https://deckofcardsapi.com/static/img/2H.png",
            value: "2",
            suit: "HEARTS",
          },
        ],
      }
    ).as("getOneCard");
  });
  describe("the functions to convert API values to display or data-blackjack-values", () => {
    it("the rankToWord function converts number ranks to words, and passes word ranks through", () => {
      cy.window().then((win) => {
        const { rankToWord } = win;
        expect(rankToWord("6")).to.equal("Six");
        expect(rankToWord("10")).to.equal("Ten");
        expect(rankToWord("ACE")).to.equal("Ace");
        expect(rankToWord("JACK")).to.equal("Jack");
      });
    });
    it("the suitToWord function converts suit symbols to words", () => {
      cy.window().then((win) => {
        const { suitToWord } = win;
        expect(suitToWord("SPADES")).to.equal("Spades");
        expect(suitToWord("HEARTS")).to.equal("Hearts");
        expect(suitToWord("DIAMONDS")).to.equal("Diamonds");
        expect(suitToWord("CLUBS")).to.equal("Clubs");
        expect(suitToWord("")).to.equal("Mystery");
      });
    });
    it("the rankToValue converts ranks to data-blackjack-values", () => {
      cy.window().then((win) => {
        const { rankToValue } = win;
        expect(rankToValue("ACE")).to.equal("11/1");
        expect(rankToValue("JACK")).to.equal("10");
        expect(rankToValue("10")).to.equal("10");
        expect(rankToValue("6")).to.equal("6");
        expect(rankToValue("Face Down")).to.equal("?");
      });
    });
  });
  it("the dealFourCards function deal the cards out as specified, and the hit and stand buttons work in a flow", () => {
    cy.wait(4000);
    cy.contains("button", "Wager").click();
    cy.wait("@getFourCards");
    cy.get(`#playersCards [data-blackjack-value="8"]`)
      .find("img")
      .should(
        "have.attr",
        "src",
        "https://deckofcardsapi.com/static/img/8D.png"
      );
    cy.get(`#dealersCards [data-blackjack-value="?"]`)
      .find("img")
      .should(
        "have.attr",
        "src",
        "https://previews.123rf.com/images/rlmf/rlmf1512/rlmf151200171/49319432-playing-cards-back.jpg"
      );
    cy.get(`#playersCards [data-blackjack-value="11/1"]`);
    cy.get(`#dealersCards [data-blackjack-value="10"]`);
    cy.contains("button", "Hit").click();
    cy.wait("@getOneCard");
    cy.get(`#playersCards [data-blackjack-value="2"]`);
    cy.contains("button", "Wager").should("not.be.visible");
    cy.contains("button", "Stand").click();
    cy.contains("button", "Wager").should("be.visible");
  });
});
