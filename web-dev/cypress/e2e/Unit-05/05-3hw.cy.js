describe("Week 5 Day 3 Blackjack app homework", () => {
  beforeEach(() => {
    cy.visit("/blackjack/table.html", {
      onBeforeLoad(win) {
        cy.spy(win.console, "log").as("consoleLog");
      },
    });
    cy.clock();
  });
  it("forces the player to play a stand after 10 second timeout", () => {
    cy.intercept(
      {
        method: "GET",
        url: "/api/deck/*/draw?count=4",
      },
      {
        cards: [
          {
            image: "https://deckofcardsapi.com/static/img/8D.png",
            value: "8",
            suit: "DIAMONDS",
          },
          {
            image: "https://deckofcardsapi.com/static/img/KC.png",
            value: "KING",
            suit: "CLUBS",
          },
          {
            image: "https://deckofcardsapi.com/static/img/9D.png",
            value: "9",
            suit: "DIAMONDS",
          },
          {
            image: "https://deckofcardsapi.com/static/img/KH.png",
            value: "KING",
            suit: "HEARTS",
          },
        ],
      }
    ).as("getFourCards");
    cy.contains("button", "Wager").click();
    cy.wait("@getFourCards");
    cy.tick(10000);
    cy.get("@consoleLog").should("be.calledWith", "player timeout");
  });
});
