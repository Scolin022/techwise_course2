describe("Week 5 Day 2 Blackjack app homework", () => {
  beforeEach(() => {
    cy.visit("/blackjack/table.html", {
      onBeforeLoad(win) {
        cy.spy(win.console, "log").as("consoleLog");
      },
    });
  });
  it("busts the player when their hand's value excedes 21", () => {
    cy.intercept(
      {
        method: "GET",
        url: "/api/deck/*/draw?count=4",
      },
      {
        cards: [
          {
            image: "https://deckofcardsapi.com/static/img/8D.png",
            value: "8",
            suit: "DIAMONDS",
          },
          {
            image: "https://deckofcardsapi.com/static/img/KC.png",
            value: "KING",
            suit: "CLUBS",
          },
          {
            image: "https://deckofcardsapi.com/static/img/9D.png",
            value: "9",
            suit: "DIAMONDS",
          },
          {
            image: "https://deckofcardsapi.com/static/img/KH.png",
            value: "KING",
            suit: "HEARTS",
          },
        ],
      }
    ).as("getFourCards");
    cy.intercept(
      {
        method: "GET",
        url: "/api/deck/*/draw?count=1",
      },
      {
        cards: [
          {
            image: "https://deckofcardsapi.com/static/img/8H.png",
            value: "8",
            suit: "HEARTS",
          },
        ],
      }
    ).as("getOneCard");

    cy.contains("button", "Wager").click();
    cy.wait("@getFourCards");
    cy.contains("button", "Hit").click();
    cy.wait("@getOneCard");
    cy.get("@consoleLog").should("be.calledWith", "player busted");
  });
  it("busts the dealer when their hand's value excedes 21", () => {
    cy.intercept(
      {
        method: "GET",
        url: "/api/deck/*/draw?count=4",
      },
      {
        cards: [
          {
            image: "https://deckofcardsapi.com/static/img/KC.png",
            value: "KING",
            suit: "CLUBS",
          },
          {
            image: "https://deckofcardsapi.com/static/img/8D.png",
            value: "8",
            suit: "DIAMONDS",
          },
          {
            image: "https://deckofcardsapi.com/static/img/KH.png",
            value: "KING",
            suit: "HEARTS",
          },
          {
            image: "https://deckofcardsapi.com/static/img/8H.png",
            value: "8",
            suit: "HEARTS",
          },
        ],
      }
    ).as("getFourCards");
    cy.intercept(
      {
        method: "GET",
        url: "/api/deck/*/draw?count=1",
      },
      {
        cards: [
          {
            image: "https://deckofcardsapi.com/static/img/6H.png",
            value: "6",
            suit: "HEARTS",
          },
        ],
      }
    ).as("getOneCard");

    cy.contains("button", "Wager").click();
    cy.wait("@getFourCards");
    cy.contains("button", "Stand").click();
    cy.wait("@getOneCard");
    cy.get("@consoleLog").should("be.calledWith", "dealer busted");
  });
  it("handles a ridiculous number of aces without busting", () => {
    cy.intercept(
      {
        method: "GET",
        url: "/api/deck/*/draw?count=4",
      },
      {
        cards: [
          {
            image: "https://deckofcardsapi.com/static/img/AD.png",
            value: "ACE",
            suit: "DIAMONDS",
          },
          {
            image: "https://deckofcardsapi.com/static/img/AD.png",
            value: "ACE",
            suit: "DIAMONDS",
          },
          {
            image: "https://deckofcardsapi.com/static/img/AD.png",
            value: "ACE",
            suit: "DIAMONDS",
          },
          {
            image: "https://deckofcardsapi.com/static/img/AD.png",
            value: "ACE",
            suit: "DIAMONDS",
          },
        ],
      }
    ).as("getFourCards");
    cy.intercept(
      {
        method: "GET",
        url: "/api/deck/*/draw?count=1",
      },
      {
        cards: [
          {
            image: "https://deckofcardsapi.com/static/img/AD.png",
            value: "ACE",
            suit: "DIAMONDS",
          },
        ],
      }
    ).as("getOneCard");
    cy.contains("button", "Wager").click();
    cy.wait("@getFourCards");
    for (let i = 0; i < 19; i++) {
      cy.contains("button", "Hit").click();
      cy.wait("@getOneCard");
    }
    cy.contains("button", "Stand").click();
  });
  it("dealer must hit on 16", () => {
    cy.intercept(
      {
        method: "GET",
        url: "/api/deck/*/draw?count=4",
      },
      {
        cards: [
          {
            image: "https://deckofcardsapi.com/static/img/KC.png",
            value: "KING",
            suit: "CLUBS",
          },
          {
            image: "https://deckofcardsapi.com/static/img/8D.png",
            value: "8",
            suit: "DIAMONDS",
          },
          {
            image: "https://deckofcardsapi.com/static/img/KH.png",
            value: "KING",
            suit: "HEARTS",
          },
          {
            image: "https://deckofcardsapi.com/static/img/8H.png",
            value: "8",
            suit: "HEARTS",
          },
        ],
      }
    ).as("getFourCards");
    cy.intercept(
      {
        method: "GET",
        url: "/api/deck/*/draw?count=1",
      },
      {
        cards: [
          {
            image: "https://deckofcardsapi.com/static/img/6H.png",
            value: "6",
            suit: "HEARTS",
          },
        ],
      }
    ).as("getOneCard");

    cy.contains("button", "Wager").click();
    cy.wait("@getFourCards");
    cy.contains("button", "Stand").click();
    cy.wait("@getOneCard");
  });
  it("dealer must stand on 17", () => {
    cy.intercept(
      {
        method: "GET",
        url: "/api/deck/*/draw?count=4",
      },
      {
        cards: [
          {
            image: "https://deckofcardsapi.com/static/img/KC.png",
            value: "KING",
            suit: "CLUBS",
          },
          {
            image: "https://deckofcardsapi.com/static/img/8D.png",
            value: "8",
            suit: "DIAMONDS",
          },
          {
            image: "https://deckofcardsapi.com/static/img/KH.png",
            value: "KING",
            suit: "HEARTS",
          },
          {
            image: "https://deckofcardsapi.com/static/img/9H.png",
            value: "9",
            suit: "HEARTS",
          },
        ],
      }
    ).as("getFourCards");
    cy.intercept(
      {
        method: "GET",
        url: "/api/deck/*/draw?count=1",
      },
      {
        cards: [
          {
            image: "https://deckofcardsapi.com/static/img/6H.png",
            value: "6",
            suit: "HEARTS",
          },
        ],
      },
      cy.spy().as("getOneCard")
    );
    cy.contains("button", "Wager").click();
    cy.wait("@getFourCards");
    cy.contains("button", "Stand").click();
    cy.get("@getOneCard").should("not.have.been.called");
  });
});
