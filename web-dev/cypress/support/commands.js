// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
Cypress.Commands.add("desktop", () => {
  cy.viewport(1000, 800);
});
Cypress.Commands.add("mobile", () => {
  cy.viewport(400, 800);
});
Cypress.Commands.add("waitForResource", (resourceName) => {
  cy.window().then((win) => {
    const foundResource = win.performance
      .getEntriesByType("resource")
      .find((item) => item.name.endsWith(resourceName));
    return foundResource;
  });
});

//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
