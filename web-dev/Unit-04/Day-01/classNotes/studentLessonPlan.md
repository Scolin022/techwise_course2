# 04.1 Javascript: Taking user input

## Overview

What will students be able to do after today?

- Students will be able to take user input using event listeners and handlers

## Goals and How We will Assess Student Learning

## Class Instructions

### Activity 0. Quiz over Readings

### Leverage Activity 1: Validate ticker search form

We will begin to shift our focus from presentational concerns to interactivity and function, and with that comes a shift to javascript.

- document querries, use html elements in javascript.
- event listeners.
- functions
- selection of the different tickers available

### Blackjack In Class Practice Activity 2: On click, deal a random card to the console

Add a listener for click events on the "Hit" button, which will deal a random card from a deck.

- Create a deck by combining symbols for the suits with symbols for the ranks.

  - use these definitions:

  ```js
  const suits = ["♠", "♡", "♢", "♣"];
  const ranks = ["Ace", 2, 3, 4, 5, 6, 7, 8, 9, 10, "Knave", "Queen", "King"];
  ```

  to define deck of cards, each of which has is an object with a suit and rank property:

  ```js
  {
    suit: "♠",
    rank: 2
  }
  ```

  - look into the `for` or `for...of` statements to accomplish this efficiently
  - declare a function in your javascript called "getDeck" that returns the deck
  - declare a function called "getRandomCard" that gets a random card from the deck

- listen for click events on the hit button
  - add an id of "hit-button" to the appropriate button element
  - on click of said button, console.log one of the cards randomly both of the following ways:
    - as a string of the form "Your card is 2 of ♠. Did you bust?" (using concatenation or template literals)
    - as the object itself
  - look into `Math.random`

### Leverage Activity 3: Validate registration

- clean up login
- form interactivity, login.
- form validation
- more functions.

### Blackjack Homework: Eligibility Form Validation

Students will add dom access and javascript logic validate the user's information when they click `Sign up`. If they don't complete, our next activity will cover the core skills here, encourage them to do their best.

- Capture and _in a separate console.log_ log each textField's label and value, separated by a colon and space, e.g.:
  - `Full Name: Mary Potter`
  - `Username: HarryPoppins`


- For the checkboxes, console.log the two statements that are true from the following four:
  - "The user has checked the legal checkbox"
  - "The user has checked the terms checkbox"
  - "The user has not checked the legal checkbox"
  - "The user has not checked the terms checkbox"
- If the following are true, console.log "The user is eligible"
  - the passwords are both the same ( use the `===` strict comparison operator)
  - the user is 13 or older(COPPA)
  - the user has checked both checkboxes
  - none of the other fields are blank
  - if any of the above are not true, log that "The user is ineligible"c

Stretch Goal:

- Compare the user's age and birthdate. If their age is accurately inaccurately assessed, console.log "the user is not likely to be good at math", otherwise log that ""the user can figure out the user's age"
  - Look into the [Date Object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date)
