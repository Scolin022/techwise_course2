# 04.3 Local Storage and Event Delegation

## Overview

What will students be able to do after today?

- Use the local storage API to persist data client-side.

## Class Instructions

### Activity 0. Quiz over Readings

### Leverage Activity 1: Update the Call To Action section

We'll enrich our story from last time

> The focus section should let us Buy, Sell, Watch, or Ignore a stock"

- abstract a class to create the material buttons.
- use the browser local storage api to save
- add Call to action functions, buy, sell, etc.

### Blackjack In Class Practice Activity 2: Persist the user's bankroll

You've had hard homework this week, here is an easy one. We want the user's bankroll to be persisted to local storage.

- Update and `setBankroll` funtions to save the bankroll value to local storage under the key `bankroll`
- Update the initial definition of the bankroll to use the value stored in local storage.
  - Fallback to 2022 as the bankroll if there are no values in local storage.

### Leverage Activity 3: Add Event Delegation to the List Items

Time to handle our other story from last time:

> Clicking on the list elements should change the focus section

- use event delegation, so you dont have to have lots of event listeners.

### Blackjack HW: NONE!

The rest of class is for questions, doubts, or breakout work finishing up any touches to the work from earlier this week.
