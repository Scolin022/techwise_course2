const MDCList = mdc.list.MDCList;
const MDCRipple = mdc.ripple.MDCRipple;
const list = new MDCList(document.querySelector(".mdc-list"));

const listItemRipples = list.listElements.map(
  (listItemEl) => new MDCRipple(listItemEl)
);

const userStocks = {
  GOOG: 10,
  AAPL: 0,
  AMZN: 0,
};

const tickerStubs = [
  { ticker: "META", price: 174.66 },
  { ticker: "AAPL", price: 174.15 },
  { ticker: "AMZN", price: 142.3 },
  { ticker: "NFLX", price: 245.17 },
  { ticker: "GOOG", price: 120.86 },
  { ticker: "GOOGL", price: 120.17 },
  { ticker: "TSLA", price: 908.61 },
  { ticker: "TWTR", price: 43.86 },
];

const tickerSearchIcon = document.querySelector("#ticker-search");
const tickerSearchInput = document.querySelector("#ticker-input");
const focusedDescription = document.querySelector("#focused-description");

function getTickerInput() {
  const searchedTicker = tickerSearchInput.value;
  if (validateTickerInput(searchedTicker)) {
    focusTicker(findStubByTicker(searchedTicker));
  } else {
    console.log(searchedTicker + " is not a stock");
  }
}
tickerSearchIcon.addEventListener("click", getTickerInput);
tickerSearchInput.addEventListener("keypress", function (event) {
  if (event.key === "Enter") {
    event.preventDefault();
    tickerSearchIcon.click();
  }
});

function validateTickerInput(tickerSymbol) {
  return tickerStubs.some(function (stub) {
    return stub.ticker == tickerSymbol;
  });
}

function createStockListItem(tickerStub) {
  const newListItem = document.createElement("li");
  newListItem.classList.add("mdc-list-item", "space-between");
  newListItem.setAttribute("role", "option");
  newListItem.setAttribute("tabindex", "0");

  const rippleSpan = document.createElement("span");
  rippleSpan.classList.add("mdc-list-item__ripple");
  newListItem.appendChild(rippleSpan);

  const textSpan = document.createElement("span");
  textSpan.classList.add("mdc-list-item__text");
  textSpan.innerText = tickerStub.ticker;
  newListItem.appendChild(textSpan);

  const usersShares = userStocks[tickerStub.ticker];
  const sharesSpan = document.createElement("span");
  if (usersShares) {
    sharesSpan.innerText = `${usersShares} shares: ${displayDollars(
      usersShares * tickerStub.price
    )}`;
  } else {
    sharesSpan.innerText = `just watching`;
  }
  newListItem.appendChild(sharesSpan);

  const priceSpan = document.createElement("span");
  priceSpan.innerText = displayDollars(tickerStub.price);
  newListItem.appendChild(priceSpan);

  const stockList = document.querySelector("ul#stock-list");
  stockList.appendChild(newListItem);
}

function findStubByTicker(searchedTicker) {
  return tickerStubs.find(function (stub) {
    return stub.ticker == searchedTicker;
  });
}

function intializeStockList() {
  Object.keys(userStocks).forEach(function (searchedTicker) {
    createStockListItem(findStubByTicker(searchedTicker));
  });
}
intializeStockList();

function displayDollars(number) {
  return `$${number.toFixed(2)}`;
}

function emptyTheNest(domNode) {
  while (domNode.firstChild) {
    domNode.removeChild(domNode.firstChild);
  }
}

function focusTicker(ticker) {
  emptyTheNest(focusedDescription);

  const stockHeadlines = document.createElement("p");
  stockHeadlines.classList.add(
    "flex-center",
    "space-between",
    "ampler-padding",
    "half-width"
  );
  const stockSymbol = document.createElement("span");
  stockSymbol.innerText = ticker.ticker;
  const stockPrice = document.createElement("span");
  stockPrice.innerText = ticker.price;
  stockHeadlines.append(stockSymbol, stockPrice);
  focusedDescription.appendChild(stockHeadlines);
  const usersShares = userStocks[ticker.ticker];

  if (usersShares) {
    const userStake = document.createElement("p");
    userStake.classList.add(
      "flex-center",
      "space-between",
      "ampler-padding",
      "half-width"
    );
    const numberOfShares = document.createElement("span");
    numberOfShares.innerText = `${usersShares} shares`;
    const valuation = document.createElement("span");
    valuation.innerText = displayDollars(usersShares * ticker.price);
    userStake.append(numberOfShares, valuation);
    focusedDescription.appendChild(userStake);
  }

  const stockDescription = document.createElement("p");
  stockDescription.innerText = `This lorem concerning ${ticker.ticker} ipsum dolor sit amet consectetur adipisicing elit. Tempore, at ullam repellendusexpedita aperiam optio, rem quos voluptate ea facere velit cumcommodi placeat nesciunt deserunt quidem. Aspernatur,repellendus nobis?`;
  focusedDescription.appendChild(stockDescription);
}
focusTicker(tickerStubs[4]);
