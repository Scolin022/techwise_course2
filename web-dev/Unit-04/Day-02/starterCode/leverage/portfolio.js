const MDCList = mdc.list.MDCList;
const MDCRipple = mdc.ripple.MDCRipple;
const list = new MDCList(document.querySelector(".mdc-list"));

const listItemRipples = list.listElements.map(
  (listItemEl) => new MDCRipple(listItemEl)
);
// Activity 1
const tickerSearchIcon = document.querySelector("#ticker-search");
// console.log(tickerSearchIcon);

// function techWiseFirstFunction() {
//   console.log("hello techWise");
//   return "returnValue";
// }
// tickerSearchIcon.addEventListener("click", techWiseFirstFunction);

const tickerSearchInput = document.querySelector("#ticker-input");
// console.log(tickerSearchInput);
// console.dir(tickerSearchInput);

function getTickerInput() {
  const searchedTicker = tickerSearchInput.value;
  if (validateTickerInput(searchedTicker)) {
    console.log(`${searchedTicker} is a stock`);
  } else {
    console.log(searchedTicker + " is not a stock");
  }
}
tickerSearchIcon.addEventListener("click", getTickerInput);

const tickerStubs = ["META", "AAPL", "AMZN", "NFLX", "GOOG", "GOOGL", "TSLA"];
function validateTickerInput(tickerSymbol) {
  return tickerStubs.includes(tickerSymbol);
}
