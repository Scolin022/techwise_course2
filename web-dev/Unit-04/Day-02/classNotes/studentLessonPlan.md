# 04.2 JavaScript: Interaction with the DOM

## Overview

What will students be able to do after today?

- Manipulate the dom to reflect user interaction.

## Class Instructions

### Activity 0. Quiz over Readings

### Leverage Activity 1: Render users tickers to the DOM upon input

- Dom manipulation, creation of html elements from javascript.
- add dynamically the list of stocks

### Blackjack ICP Activity 2: Display the card when you get a hit.

#### Brief excursion into agile

Most users don't open the console. Some might panic if they had to look there for feedback ,or opened the devTools accidentally, perhaps assuming they'd been hacked or that events of eschatological import were imminent `console.log('the end is near')`.

Users, including business stakeholders and project managers, will speak of the web at the level they interact with it. They might tell us a "user story", such as this one:

> Given that I'm a blackjack player who hasn't busted yet, when I click hit, then I expect to see a representation of my new card displayed.

Honestly, as a developer, talking at this level is convenient because it allows us abstract away our implementation concerns while we focus on clarifying requirements of a user's experience at the project planning and management level, perhaps in a "sprint planning" ceremony. Later we can come back and establish that we need to manipulate the dom to give the user feedback regarding their input, further decomposing the ask into tasks until they are sufficiently granular and defined to distribute work, perhaps in a "technical grooming" ceremony. We might establish other meetings: a daily "stand-up" or "scrum" meeting to check-in on progress and blockers, a "sprint review" or "playback" meeting to demo progress to stakeholders and other collaborators, a "retrospective" to talk/vent about what worked (not so) well, what was frustrating, action items (buy ice creams for the team!), a "backlog refinement" or "grooming" to get stories ready for the next iteration of sprint planning, etc.

We will enrich your user story with a few technical specifications:

- change the onclick event handler logRandomCard to dealRandomCard, and change the validated console log to call a new function `dealToDisplay` , passing the random card :

```js
function dealRandomCard() {
  dealToDisplay(getRandomCard());
}
```

- dealToDisplay will construct a representation of a card to display. Since right now cards are represented as list items with a data-blackjack-value and a text node describing the `<Rank> of <Suits>`, let's stay consistent with that. Changing our representation would be another (user) story.
  - We need a way to convert our ranks to number words ( 6= >"six" ), as well as suitSymbols to suit words ("♡"=>"Hearts") for the text
    - Define a function `rankToWord` that checks if a rank is a number, and converts it to a capitalized number word if so
      - Feel free to use this data structure:
        `const mapRanksToWords = { 2: "Two", 3: "Three", 4: "Four", 5: "Five", 6: "Six", 7: "Seven", 8: "Eight", 9: "Nine", 10: "Ten", }; `
        - thusly: `const twoAsAString = mapRanksToWords[2]`
    - Define an object data structure `mapSuitsToWords` used by a function `suitToWord` to map suits as keys to their English descriptions as values, similarly to the above data struture
      - it should map empty string `""` to "Mystery"
  - We need a way to convert our ranks to data-blackjack-value
    - Define a function `rankToValue` that to accomplish thusly:
      - Face cards must return "10", Aces "11/1", and the dealer's `"Face Down"` card string value returns `"?"`. All other cards return their nominal value as a string.

### Leverage Activity 3: Begin the Epic task of tracking user shares

we want to achieve the epic accomplishment of letting the user track shares of stocks that they purchase.

- track bought stocks
- create a focus section for the stocks that we have, and or track

### Blackjack Homework: Add betting

> As a blackjack player,
> I want a betting interface,
> so that I can wager a part of my bankroll

- Add a bankroll to the data model (the bankroll does not represent the players total liquid assets, but rather the money they are willing to risk in the casino)
  - Add a function (to the global scope) `getBankroll()` that gets the value of some variable defined somewhere and returns a number
  - Add a function (to the global scope) `setBankroll(newBalance)` that assigns the value of newBalance (which should be an integer) to the variable the prior function accessed
  - Initialize the player's bankroll to 2022, it's their luck year
    - As a pretend online casino, we only deal in whole dollars (integers)
- Add a function (to the global scope) `timeToBet()` that
  - Hides the #playersActions section, by adding some class whose rule contains the declaration `display: none`
  - Displays the #betting section interface you will add to the html which contains:
    - A span with display of the user's bankroll (with a `$` in front)
    - A number material textfield `#users-wager` for the user's wager
    - A material button that contains the text "Bet" and calls
      - A function called `makeWager`, that console.logs the amount in the #users-wager input and calls a timeToPlay
    - initially the #betting section should be set to `display:none`
- Add a function (to the global scope) `timeToPlay()` that
  - Displays the #playersActions section by adding a class, and hides the #betting section
