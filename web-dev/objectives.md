# Web Dev Objectives

At the end of the course, the successful participants will be able to:

- build full-stack web applications

## Unit 01: HTML/SSH

- Day 1: IDEs, ssh, running cypress grader
- Day 2: HTML tags and Semantics, Chrome Dev Tools, and Emmet
- Day 3: Linking and HTML attributes

## Unit 02: CSS

- Day 1: Legacy Layout: position, float, display
- Day 2: Modern Layout: Flexbox and Grid
- Day 3: Adding Typography: Inheritance, Cascade, Specificity

## Unit 03 Advanced and 3rd party CSS

- [Unit 3/Day 1](/Unit-03/Day-01/README.md): Adding Color: CSS custom properties, psuedo-classes and psuedo-elements
- [Unit 3/Day 2](/Unit-03/Day-02/README.md): Material Design: Intro to components
- [Unit 3/Day 3](/Unit-03/Day-03/README.md): Material Design: 12-column layouts

## Unit 04: JavaScript WebAPIs

- [Unit 4/Day 1](/Unit-04/Day_1/README.md): Taking User Input: JS forms and validation
- [Unit 4/Day 2](/Unit-04/Day_2/README.md): Reflecting User Input: Manipulation of the DOM
- [Unit 4/Day 3](/Unit-04/Day_3/README.md): JSON and local storage
