# Cypress Configuration

In order to use cypress on your local machine to test against your deployed site on portfolios.talentsprint.com, there are three principal concerns:

1. Cypress must be installed:
   - please use the direct download installation for now rather than npm
   - (we will teach you npm in 5 units)
2. Cypress must be able to find the tests.
3. Cypress must be configured to test against portfolios.talentsprint.com

In order to address the second and third concerns, let's understand a bit about cypress in general.

## Required Directory Structure

Any project can be dropped into the launchpad: https://docs.cypress.io/guides/getting-started/opening-the-app#The-Launchpad

If that project's directory were called "X", in order for cypress to run the tests then "X" would need to contain a folder called "cypress" that would contain the tests (inside a subfolder called "e2e"), as well as a file called "cypress.config.js" that is the most basic way to configure cypress.

![example structure with course-2 as X ](./course2asX.png)

If cypress.config.js were not there, cypress app will create it to help you scaffold the app. If the cypress folder were not there, cypress app will create it with some sample tests to help you understand cypress. (This behavior is meant to help you but it may be confusing)

You may substitute X for:

- "DayOneRepo"
- the "course-2" class repo we will push to gitlab
- or any other folder to run cypress,

but depending on your choice you may have to move files, or else deal with git conflicts if you use "course-2". Using the .gitignore file may solve many of these git conflicts, investigate it!

## Pointing your app at portfolios.talentsprint.com

You must edit cypress.config.js to provide the `baseUrl` that will be tested against. Your file should look like this except your username will come after the ~, instead of mine:
![example cypress.config.js](./exampleCypressConfigJs.png)

## Debugging

If you are not having success, please read the error message to help deduce what step is going wrong (if you are lucky enough to get an error message).

A common problem is that the cypress.config.js that you are editing is not the one the application is using to configure itself. Perhaps you dropped the cypress folder rather than the class repo into the launchpad, and now you have nested cypress folders. In order to assess if the file you are testing is being used by the application, please point at google (`baseUrl: https://www.google.com`) to sanity check. ![sanity check by pointing at google](./sanityCheck.png)

Here is an example result of a misconfigured cypress.config.js (in this case I set baseURL, notice the capitalization is wrong):
![check that spelling on your property names](./misconfiguredbaseURL.png)
When the configuration file is wrong, the path cypress requests files from will also be wrong. In order to test against the portfolios.talentsprint.com server, we should definitely be testing over `https`. Here's an example where I've misspelled my username, but at least the property name is right: ![oops, that's not my name](./misspelledUsername.png)

Hopefully this helps everyone identify which problem they are having with cypress, or if the problem could with something else (the server, the code, etc).

Please investigate the cypress documentation for further detail : https://docs.cypress.io/guides/end-to-end-testing/testing-your-app#Step-3-Configure-Cypress
