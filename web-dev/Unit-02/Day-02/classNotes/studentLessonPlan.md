# 02.2 Modern Layout: Grid and Flexbox

## Overview

By the end of class, students will be able to:

- Recreate and improve their layouts from yesterday with Flexbox and Grid

## Class Instructions

### Activity 0. Quiz over Readings

### Activity 1: Leverage App Shell with Grid and Flexbox

Many issues arise when we attempt to create a responsive layout with only legacy methods. Modern tools will simplify our attempt to achieve responsiveness, reducing the amount of code we have to write and understand, and making it less brittle. Enter css grid.

- comment out any @media.
- create header as a grid
  - logo, nav, account as their own column
  - addaptive width to the column width

'Better responsivity with less code - everybody wins! Let's flex our flexbox muscles to improve even more:'

- play around with flexbox

### Activity 2: Blackjack

Work on the blackjack app shell from last class to make it responsive, **Use Flexbox and optional grid.** Try not to use media queries.

We will have fewer requirements for this one, to encourage students to approach design their own way:

- sketch a wireframe for 2 minutes
- the header and footer needs to have a fixed position.
- the nav must be a flex container
- beyond this use flexbox or grid to create an awesome design. Have fun!

Invite a few students to share what they created.

### Leverage Activity 3: Add media queries to create a mobile layout

While flexbox and grid solve some responsiveness problems, there is still some problems with how it looks in different devices, ex: shrinking element.

- Look at different width to identify where are some key breakpoints
- create burger menu icon
- changes to how to display certain items.

<br>

---

## Homework:

### Blackjack Homework: Media queries

Students will add flex and/or grid to media queries to make blackjack more responsive. This is an open-ended activity, with one hoop to jump through:

- Make a breakpoint somewhere between 500-900px. Work in common.css so it applies to both
  - Make an element with a class attribute of `disappearing` that is only displayed above the breakpoint.
  - Make an element with a class attribute of `mobileFeature` that is only displayed below the breakpoint.
- Remake the about page main content to use a grid, but you can choose your own wireframe design!
