# Unit 02 Day 1 Legacy Layout: position, float, display

## Overview

By the end of class, students will be able to:

- lay out an html page using the box model, display, positioning, and media queries

## Class Instructions

### Activity 0. Quiz over Readings

### Leverage Activity 1: Using DevTools to explore Styling Layout

Show students the app shell wireframe.

- Play around Chrome Dev tools
  - Change some html/css
- https://meyerweb.com/eric/tools/css/reset/ into reset.css
- Create css style, edit the following html elements:
  - header
  - nav
  - footer
  - main
  - #copyright
  - #logo
  - #account
  - #login

### Blackjack ICP Activity 2: Make an App Shell

The students will add an app shell to their blackjack app. They are allowed to pursue their own styling to some extent, but must meet the following requirements:

- link tags in the head to
  - `reset.css`
  - `common.css`
- header
  - Set header to the top in a fixed position
- footer
  - Set the footer to a fix position in the bottom
  - a span with an `id="copyright"`
    - copyright to the bottom rigth inside the footer.
- header and footer have the same `background-color` value

### Activity 3: Leverage Media Queries

- do you like how it looks on mobile? NO
  - use device mode in chrome devTools
- great quote from Mozilla

  > "In the early days of responsive design, many
  > designers would attempt to target very specific screen sizes. Lists of the sizes
  > of the screens of popular phones and tablets were published in order that
  > designs could be created to neatly match those viewports. There are now far too
  > many devices, with a huge variety of sizes, to make that feasible. This means
  > that instead of targeting specific sizes for all designs, a better approach is
  > to change the design at the size where the content starts to break in some way.
  > Perhaps the line lengths become far too long, or a boxed out sidebar gets
  > squashed and hard to read. That's the point at which you want to use a media
  > query to change the design to a better one for the space you have available.
  > This approach means that it doesn't matter what the exact dimensions are of the
  > device being used, every range is catered for. The points at which a media query
  > is introduced are known as breakpoints."
  > https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Media_queries

- Create Two Sections, ids: asset-focus and asset-list, and create a css for this.
- to fix some responsiveness:
  - hide the account section for whenever we are below 1200 and take the nav over 200 to the left
  - hide the logo below 840px

### Activity 4: Introduce Blackjack HW

The students must recreate the `about` page main element's layout:

- add a link to a new about.css file with styles specific to this page (after reset and common stylesheet links)
- create a section with an id of `hero` that has 100% width, `height: 30vh` and
  - an h1 with a tagline including the word `Blackjack`
- create a section with an id of `whyUs` that has 40% width, is floated
  - becomes 100% when the screen is 800px or smaller
  - includes a paragraph about why we are the best blackjack app (containg the word `Why`)
- create a section with an id of `callToAction` with
  - becomes 100% when the screen is 800px or smaller
  - includes a paragraph containing the word `Money`
  - includes a button that says `Start Playing`
