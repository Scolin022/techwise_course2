# 02.3 CSS Typography

## Overview

By the end of class, students will be able to:

- understand the application of css styles according to specificity and inhertance
- gain practice styling with css

## Class Instructions

### Activity 0. Quiz over Readings

### Leverage Activity 1: CSS Fonts: Introducing Cascade, Specificity, Inheritance

- Go to [google fonts](https://fonts.google.com/) and find `poppins` and `raleway`. Add a 100,400,700 poppins and 100,400,700,900 raleway and grab the link. Add it to index.html

- play around with DevTools:
  - play around with css style, find how specificity play a role on what it shows up.
- Fonts.

### Blackjack ICP Activity 2: Add CSS Fonts

Pick two google fonts to add to your app.

- Add the link from google fonts
- Use one font for headings, but use a different font for other elements (we will check a paragraph)
- Adjust your font sizes and add some margins
  - Make sure h1 is 40px with a margin of 20px
  - #copyright needs to be 10px
- Make it look good!

### Discussion Activity 3: Consolidate CSS knowledge

- box model
- position
- display
- flexbox
- grid
- css specificity, inheritance, cascade
- other stuff exposed in the readings
- whatever else the students come up with

### 4. Homework and Questions

Happy Friday- No new homework! Make sure you have caught up on all your homework and in-class-practice activities and passed their associated assessments, and gotten a head start on readings, so that you are ready for the _fast pace_ of next week!
