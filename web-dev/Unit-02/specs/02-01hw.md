# Unit 2 Day 1 Blackjack Homework: recreate about page main element's layout

Recreate the `about` page main element's layout.

- Add a link to a new `about.css` file with styles specific to this page (after reset and common stylesheet links)
- Create a section with an id of `hero` that:
  - Has 100% width, height: 30vh
  - An h1 with a tagline including the word "Blackjack"
- Create a section with an id of `whyUs` that:
  - Has 40% width, and is floated
  - Becomes 100% when the screen is 800px or smaller
  - Includes a paragraph about why we are the best blackjack app (containg the word "Why")
- Create a section with an id of `callToAction` that:
  - Takes up the remainder of the screen width not used by #whyUs
  - Becomes 100% when the screen is 800px or smaller
  - Includes a paragraph containing the word "Money"
  - Includes a button that says "Start Playing"

Run this against the cypress test `02-1hw.cy.js`
