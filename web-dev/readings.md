# Assigned Readings

## Unit 1 - Intermediate HTML concepts and Linux Server

- Day 1: Setting up the server and basic index.html

  - https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/Installing_basic_software
  - install vscode from https://code.visualstudio.com/docs/setup/windows or https://code.visualstudio.com/docs/setup/mac or https://code.visualstudio.com/docs/setup/linux
  - watch https://code.visualstudio.com/docs/introvideos/basics
  - install chrome https://www.google.com/chrome/downloads/
  - install cypress https://docs.cypress.io/guides/getting-started/installing-cypress#Direct-download
  - https://developer.chrome.com/docs/devtools/overview/
  - https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/How_the_Web_works

- Day 2: Emmet/Page layout with Semantics

  - https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Getting_started
  - watch https://www.youtube.com/watch?v=vAAzdi1xuUY&list=PLNYkxOF6rcICWx0C9LVWWVqvHlYJyqw7g&index=20
  - https://developer.mozilla.org/en-US/docs/Learn/Accessibility/HTML

- Day 3: HTML anchors, 3rd party content (images)
  - https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Creating_hyperlinks
  - https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Images_in_HTML

## Unit 2 Styling Basics

- Day 1: Intro to CSS and Legacy Layout: selectors and rules; flow, position, box model

  - https://developer.mozilla.org/en-US/docs/Learn/CSS/First_steps/Getting_started
  - https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Introduction
  - https://developer.chrome.com/docs/devtools/css/

- Day 2: Modern Layout Tools: flexbox and grid

  - play https://flexboxfroggy.com/
  - https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Flexbox
  - play https://cssgridgarden.com/
  - https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Basic_Concepts_of_Grid_Layout

- Day 3: CSS Typography
  - https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Cascade_and_inheritance
  - study https://specifishity.com/
  - scan https://developer.mozilla.org/en-US/docs/Web/CSS/Cascade

## Unit 3: Responsive and Material Design

- 3.1: psuedo-classes, psuedo-elements, variables, animations

  - https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties
  - https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Selectors/Pseudo-classes_and_pseudo-elements

- 3.2: Material Design

  - https://m2.material.io/design/introduction
  - study https://m2.material.io/design/environment/surfaces.html#material-environment
  - check out 2 of https://m2.material.io/design/material-studies/about-our-material-studies.html

- 3.3: Material Layout
  - https://m2.material.io/design/layout/responsive-layout-grid.html#columns-gutters-and-margins
  - https://m2.material.io/develop/web/theming/theming-guide#step-2-use-the-mdc-web-colors-in-your-own-markup
  - scan https://sass-lang.com/guide

## Unit 4: WebAPIs: Taking and reflecting User Input

- Day 1: Intro to JS, Forms and js validation

  - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Grammar_and_Types
  - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration
  - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Working_with_Objects

- Day 2: Interaction with the DOM

  - https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction
  - https://developer.mozilla.org/en-US/docs/Web/API/Document_object_model/Using_the_W3C_DOM_Level_1_Core
  - https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Traversing_an_HTML_table_with_JavaScript_and_DOM_Interfaces
  - https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Building_blocks/Events#event_bubbling_and_capture

- Day 3: JSON, local storage
  - https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/JSON
  - https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API/Using_the_Web_Storage_API

## Unit 5: 3rdPartyAPIs: Fetching Data

- Day 1: Fetching a REST endpoint

  - https://developer.mozilla.org/en-US/docs/Glossary/REST
  - https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
  - https://developer.mozilla.org/en-US/docs/Web/API/fetch

- Day 2: Web Socket

  - https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_client_applications
  - https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_servers

- Day 3: Event-Driven Asynchronous JavaScript and the stack: timeouts and tics, callback => promises => await

  - https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Asynchronous/Introducing
  - https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Asynchronous/Promises
