# 05.1 Fetch

## Goals and How We will Assess Student Learning

## Class Instructions

### Leverage Activity 1: Fetching Stock Data

it is time to reach outside of our website and fetch data from other sources.

- Open the Finnhub.io homepage https://finnhub.io/
- Open the stock symbol endpoint https://finnhub.io/docs/api/quote
- Open the basic fetch example https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch

- dynamically add stock prices to DOM

### Blackjack In Class Practice Activity #2: Fetch some cards

- Create a deck of cards by hitting the `https://www.deckofcardsapi.com/api/deck/new/shuffle` endpoint with 6 decks, as is traditional for blackjack
  - Do this in a function called `getShoe` (the shoe holds the six shuffled decks) with a callback as the only parameter, and after parsing the data as json passes it to the callback
- Call getShoe in your code to set a variable `deckId` with the deck_id that is returned
- Draw four cards from the draw endpoint `https://www.deckofcardsapi.com/api/deck/<<deck_id>>/draw`
  - create a function called `drawFourCards` with a callback as the only parameter, that hits the endpoint, parses the response, and then passes the array of four cards as an argument to the callback
    - make sure you use the proper deck_id, so that cards are being drawn from the same deck
    - you can test the function in the console, but don't call it in your code (only call getShoe with some callback that sets the deckId)

### Leverage Activity #3: Search Cryptos, Introduce Error Handling

We need to add the ability to search cryptos as well as stocks.

- binance url https://docs.binance.us/#introduction
- https://docs.binance.us/#price-data
- add the ability to switch between crypto and stocks

### Blackjack Homework:

- Adjust your data model to reflect this API's.

  - Fetch a new, sorted deck from this endpoint: https://www.deckofcardsapi.com/api/deck/new/
  - Analyze this deck to determine how your suits and ranks should be redefined.
    - OK, from now on, let's call the knaves jacks.
  - Update the mapping functions:
    - `rankToWord`
    - `suitToWord`
    - `rankToValue`

- Create a function called "dealFourCards" as callback for drawFourCards function, that deals the first and third card to the player, the second card to the dealer face down, and the fourth card face up to the dealer

  - Use the card images to reflect the drawn cards to the DOM

    - Use this for the face down card `https://previews.123rf.com/images/rlmf/rlmf1512/rlmf151200171/49319432-playing-cards-back.jpg`;
    - Make sure the alt attributes are set to use values returned by rankToWord and suitToWord
      - eg. `Ace of Hearts` or `Ten of Diamonds`
    - Feel free to style the images somewhat

  - This function should be called inside of `timeToPlay`

- Implement the hit functionality to deal the player one additional card at a time
  - No need to check for busting.
- On click of the "Stand" button, call `timeToBet`
    <!-- - create a mobile version that fans/stacks the cards using z-index so you can still see their corner values -->
