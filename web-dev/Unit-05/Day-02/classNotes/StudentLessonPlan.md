# 05.2 Websockets

## Overview

What will students be able to do after today?

- Create basic client-side websocket

## Goals and How We will Assess Student Learning

## Class Instructions

### Activity 1: Basic Crypto Websocket

- Websocket, no more pain of calling a thousand time the REST API

### Leverage Activity 2: Adjust Data Model

- adjust data model to be aware of stock and cryptos.
- reformat code because of the previous point.

### Leverage Activity 3: Refactor towards client side MVC.

Using Model View Controller pattern to simplify our 300+ lines of brain melting code.

### Blackjack Homework: Implement the working game with hit and stand

Implement the following key features of the working game of blackjack based off of the description at https://bicyclecards.com/how-to-play/blackjack/
In order to allow you to choose your own UX, we will spy on console.logs to know what happened.

- The player should be able to choose "Hit" or "Stand"
- The player should be able to continue to hit until they bust
  - Log "player busted" to the console
  - Aces should be able to be counted as 11 until they must be counted as 1, therefore:
    - Your logic should be able to handle a ridiculous number of aces in the deck without busting
- The player should be able to choose to stand, and then it becomes the dealer's turn. The dealer's play is dictated by their cards as described in the link
  - The dealer should be able to bust
  - The dealer must hit on 16
  - The dealer must stand on 17
