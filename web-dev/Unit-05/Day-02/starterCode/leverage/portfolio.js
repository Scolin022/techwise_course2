const MDCList = mdc.list.MDCList;
const MDCRipple = mdc.ripple.MDCRipple;
const list = new MDCList(document.querySelector(".mdc-list"));
const listItemRipples = list.listElements.map(
  (listItemEl) => new MDCRipple(listItemEl)
);
//add this Lev2
const MDCTextFieldHelperText = mdc.textField.MDCTextFieldHelperText;
const helperText = new MDCTextFieldHelperText(
  document.querySelector(".mdc-text-field-helper-text")
);
//end add Lev2
const userStocks = JSON.parse(localStorage.getItem("userStocks")) || {
  GOOG: 1,
  AAPL: 0,
  AMZN: 0,
};

function refreshPage(stockInFocus) {
  focusTicker(stockInFocus);
  setStorage();
  refreshAssetList();
}

function buyStock(stock) {
  if (userStocks[stock.ticker]) {
    userStocks[stock.ticker] += 1;
  } else {
    userStocks[stock.ticker] = 1;
  }
  refreshPage(stock);
}
function sellStock(stock) {
  userStocks[stock.ticker] -= 1;
  refreshPage(stock);
}
function watchStock(stock) {
  userStocks[stock.ticker] = 0;
  refreshPage(stock);
}
function ignoreStock(stock) {
  if (userStocks[stock.ticker]) {
    alert(" it is prudent to sell before you ignore! ");
  } else {
    delete userStocks[stock.ticker];
  }
  refreshPage(stock);
}

const stockStubs = [
  { ticker: "META", price: 174.66 },
  { ticker: "AAPL", price: 174.15 },
  { ticker: "AMZN", price: 142.3 },
  { ticker: "NFLX", price: 245.17 },
  { ticker: "GOOG", price: 120.86 },
  { ticker: "GOOGL", price: 120.17 },
  { ticker: "TSLA", price: 908.61 },
  { ticker: "TWTR", price: 43.86 },
];
// Add this part
const myToken = "cc3nfp2ad3i9vsk3u7gg";
function fetchStockPrice(stockSymbol, callback) {
  fetch(
    `https://finnhub.io/api/v1/quote?symbol=${stockSymbol}&token=${myToken}`
  )
    .then((response) => response.json())
    .then((data) => {
      if (data.c == 0) throw "Oh no! Probably it is not a stock";
      callback(data);
      hideSearchError();
    })
    .catch((thrownError) => displaySearchError(thrownError));
}
function updateElementAssetPrice(element, stockSymbol) {
  fetchStockPrice(stockSymbol, function (stockQuote) {
    element.innerText = displayDollars(stockQuote.c);
  });
}
function updateElementAssetValuation(
  element,
  stockSymbol,
  numShares,
  isPrefixed = true
) {
  fetchStockPrice(stockSymbol, function (stockQuote) {
    const prefix = isPrefixed ? `${numShares} shares:` : "";
    const displayText = prefix + displayDollars(stockQuote.c * numShares);
    element.innerText = displayText;
  });
}
// end add Lev1

// add Lev2
function fetchCryptoPrice(crytpoSymbol, callback) {
  const baseUrl = "https://api.binance.us";
  fetch(baseUrl + `/api/v3/ticker/price?symbol=${crytpoSymbol}`)
    .then((data) => data.json())
    .then((data) => {
      callback(data);
      hideSearchError();
    })
    .catch((_) => displaySearchError("Sure that's a crypto?"));
}
let isSearchingCryptos = false;
const focusHeadline = document.querySelector("#focus-headline");
const toggleButton = document.querySelector("#toggle-focus");
function toggleSearchMode() {
  focusHeadline.innerText = isSearchingCryptos
    ? "Searching Stocks"
    : "Searching Cryptos";
  toggleButton.childNodes[3].innerText = isSearchingCryptos
    ? "Search Cryptos"
    : "Search Stocks";
  isSearchingCryptos = !isSearchingCryptos;
}
toggleButton.addEventListener("click", toggleSearchMode);

const searchHelperText = document.querySelector("#helper-text");
function displaySearchError(errorMessage) {
  searchHelperText.innerText = errorMessage;
  searchHelperText.classList.add("mdc-text-field-helper-text--validation-msg");
  searchHelperText.classList.remove("mdc-text-field-helper-text");
}
function hideSearchError() {
  searchHelperText.innerText = "";
  searchHelperText.classList.remove(
    "mdc-text-field-helper-text--validation-msg"
  );
  searchHelperText.classList.add("mdc-text-field-helper-text");
}
// end add Lev2

const tickerSearchIcon = document.querySelector("#ticker-search");
const tickerSearchInput = document.querySelector("#ticker-input");
const focusedDescription = document.querySelector("#focused-description");
const focusedCTAs = document.querySelector("#focused-ctas");
const stockList = document.querySelector("ul#stock-list");
stockList.addEventListener("click", focusListItem);

function getTickerInput() {
  const searchedTicker = tickerSearchInput.value;
  if (validateTickerInput(searchedTicker)) {
    isSearchingCryptos
      ? fetchCryptoPrice(searchedTicker, console.log)
      : fetchStockPrice(searchedTicker, console.log);
  } else {
    console.log("they're searching nothing");
  }
}
tickerSearchIcon.addEventListener("click", getTickerInput);
tickerSearchInput.addEventListener("keypress", function (event) {
  if (event.key === "Enter") {
    event.preventDefault();
    tickerSearchIcon.click();
  }
});
// adjust Lev2
function validateTickerInput(tickerSymbol) {
  return tickerSymbol !== "";
}
// end adjust Lev2
function createAssetListItem(stock) {
  const newListItem = document.createElement("li");
  newListItem.classList.add("mdc-list-item", "space-between");
  newListItem.setAttribute("role", "option");
  newListItem.setAttribute("tabindex", "0");
  newListItem.setAttribute("data-ticker", stock.ticker);

  const rippleSpan = document.createElement("span");
  rippleSpan.classList.add("mdc-list-item__ripple");
  newListItem.appendChild(rippleSpan);

  const textSpan = document.createElement("span");
  textSpan.classList.add("mdc-list-item__text");
  textSpan.innerText = stock.ticker;
  newListItem.appendChild(textSpan);
  const usersShares = userStocks[stock.ticker];
  const sharesSpan = document.createElement("span");
  if (usersShares) {
    //adjust Lev1 here
    sharesSpan.innerText = "Loading ... ";
    updateElementAssetValuation(sharesSpan, stock.ticker, usersShares);
    //end adjust Lev1
  } else {
    sharesSpan.innerText = `just watching`;
  }
  newListItem.appendChild(sharesSpan);
  const priceSpan = document.createElement("span");
  //adjust Lev1 here
  priceSpan.innerText = " Loading ...";
  newListItem.appendChild(priceSpan);
  updateElementAssetPrice(priceSpan, stock.ticker);
  stockList.appendChild(newListItem);
  //end adjust Lev1
}

function findStubByTicker(searchedTicker) {
  return stockStubs.find(function (stub) {
    return stub.ticker == searchedTicker;
  });
}

function refreshAssetList() {
  removeChildren(stockList);
  Object.keys(userStocks).forEach(function (searchedTicker) {
    createAssetListItem(findStubByTicker(searchedTicker));
  });
}
refreshAssetList();

function displayDollars(number) {
  return `$${number.toFixed(2)}`;
}

function removeChildren(domNode) {
  while (domNode.firstChild) {
    domNode.removeChild(domNode.firstChild);
  }
}

function makeMaterialButton(buttonAction) {
  const newButton = document.createElement("button");
  newButton.classList.add(
    "mdc-button",
    "mdc-card__action",
    "mdc-card__action--button"
  );

  const rippleDiv = document.createElement("div");
  rippleDiv.classList.add("mdc-button__ripple");
  newButton.appendChild(rippleDiv);

  const labelSpan = document.createElement("span");
  labelSpan.classList.add("mdc-button__label");
  labelSpan.innerText = buttonAction;
  newButton.appendChild(labelSpan);

  return newButton;
}

function focusTicker(stock) {
  removeChildren(focusedDescription);
  removeChildren(focusedCTAs);

  const stockHeadlines = document.createElement("p");
  stockHeadlines.classList.add(
    "flex-center",
    "space-between",
    "ampler-padding",
    "half-width"
  );
  const stockSymbol = document.createElement("span");
  stockSymbol.innerText = stock.ticker;
  const stockPrice = document.createElement("span");
  //adjust Lev1 here
  stockPrice.innerText = "Loading";
  updateElementAssetPrice(stockPrice, stock.ticker);
  //end adjust Lev1
  stockHeadlines.append(stockSymbol, stockPrice);
  focusedDescription.appendChild(stockHeadlines);
  const usersShares = userStocks[stock.ticker];
  const buyButton = makeMaterialButton("Buy");
  buyButton.addEventListener("click", function () {
    buyStock(stock);
  });
  focusedCTAs.appendChild(buyButton);

  if (usersShares) {
    const userStake = document.createElement("p");
    userStake.classList.add(
      "flex-center",
      "space-between",
      "ampler-padding",
      "half-width"
    );
    const numberOfShares = document.createElement("span");
    numberOfShares.innerText = `${usersShares} shares`;
    const valuation = document.createElement("span");
    //adjust Lev1 here
    valuation.innerText = "Loading...";
    updateElementAssetValuation(valuation, stock.ticker, usersShares, false);
    //end adjust Lev1
    userStake.append(numberOfShares, valuation);
    focusedDescription.appendChild(userStake);
    const sellButton = makeMaterialButton("Sell");
    sellButton.addEventListener("click", function () {
      sellStock(stock);
    });
    focusedCTAs.appendChild(sellButton);
  }

  if (Object.keys(userStocks).includes(stock.ticker)) {
    const ignoreButton = makeMaterialButton("Ignore");
    ignoreButton.addEventListener("click", function () {
      ignoreStock(stock);
    });
    focusedCTAs.appendChild(ignoreButton);
  } else {
    const watchButton = makeMaterialButton("Watch");
    watchButton.addEventListener("click", function () {
      watchStock(stock);
    });
    focusedCTAs.appendChild(watchButton);
  }

  const stockDescription = document.createElement("p");
  stockDescription.innerText = `This lorem concerning ${stock.ticker} ipsum dolor sit amet consectetur adipisicing elit. Tempore, at ullam repellendusexpedita aperiam optio, rem quos voluptate ea facere velit cumcommodi placeat nesciunt deserunt quidem. Aspernatur,repellendus nobis?`;
  focusedDescription.appendChild(stockDescription);

  tickerSearchInput.value = stock.ticker;
}

focusTicker(stockStubs[4]);

function setStorage() {
  localStorage.setItem("userStocks", JSON.stringify(userStocks));
}

stockList.addEventListener("click", focusListItem);
function focusListItem(event) {
  let nodeOfInterest = event.target;
  if (nodeOfInterest.matches("ul")) return;
  while (!nodeOfInterest.matches("li")) {
    nodeOfInterest = nodeOfInterest.parentNode;
  }
  const dataAttribute = nodeOfInterest.dataset["ticker"];
  focusTicker(findStubByTicker(dataAttribute));
}
