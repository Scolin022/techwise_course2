# 03.2 Using Material Components

## Overview

What will students be able to do after today?
Students will be able to layout a more complexly designed site with Material

## Class Instructions

### Activity 0. Announcements and Quiz over Readings

### Activity 1: Introduce Material

intro to Material

- design philosophy and implementation.
  - https://material-components.github.io/material-components-web-catalog/#/component/button
  - https://material.io/components/buttons
- quick start, https://material.io/develop/web/getting-started#quick-start-cdn
- https://material.io/develop/web/guides/importing-js#global--cdn
- https://material.io/develop/web/guides/importing-js#global--cdn

- some javascript interactivity for material.

### Leverage Activity 2: use Material to create login and registration forms.

#### Always look to implementations from material.io for consistency.

let's implements some material.io to the login

- different order of imports. Material and common.css

### Leverage Activity 3: Build out leverage app shell with material basic components.

We will rebuild the app shell using material.

- [theming guide](https://material.io/develop/web/theming/theming-guide#step-2-use-the-mdc-web-colors-in-your-own-markup)
- material theme
- material classes
- reflect on how much we gain using material

### 4. Blackjack Homework: Rebuild the app shell with material.

Begin homework with the time left in class since there is no in class practice exercise today.

The students will recreate the app shell for their blackjack app in a file shell.html. They are allowed to pursue their own styling to some extent, but must meet the following requirements:

- exactly 4 link tags in the head, in this order:
  - `reset.css`
  - material CDN link
  - material icon link
  - `shell.css` for custom styles
- exactly 2 script tags:
  - one in the head for material cdn
  - one in the body pointing to `shell.js`, which will have all code to initialize the components
- header
  - needs to be a material top app bar
- nav containing exactly 3 anchors, in order, linking to
  - `index.html`
  - `rules.html`
  - `table.html`
- footer
  - a span with an `id="copyright"`
- header and footer have the same `background-color` value
  - using the appropriate `mdc-` custom properties
