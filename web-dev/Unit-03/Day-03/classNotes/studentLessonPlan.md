# 03.3 Material Layout

What will students be able to do after today?
Students will understand the material layout system

## Class Instructions

### Activity 0. Quiz over Readings

### Leverage Activity 1: Layout the portfolio page with material layout

- [Material layout grid](https://material.io/develop/web/supporting/layout-grid)
- 12 column layout.

- Let's make the list a list! https://material.io/components/lists/web.
- add a card with some action buttons and icons

- text field, leading and trailing icons

### Blackjack In Class Practice Activity 2: Layout table.html

Redesign the table.html using your app shell.

- Make the sections #dealersCards, #playersCards, and #playersAction all material card components by giving them the class "mdc-card"
  - Don't worry too much about the card values for now, we don't need to style them like cards. Just a text node inside an ordered list item is sufficient.
  - Just make sure one of the dealers cards is `Face Down`
  - Put them in a material grid with a declaration that they will span some value of columns
- The #playersActions sections should have four material buttons with the following contents:
  - `Hit`
  - `Stand`
  - `Split`
  - `Insure`
- Make it look as good as you want :)
  (45 min)

Since we were so activity heavy last time, let's spend some time answering questions and doubts.

### Blackjack HW: Create an eligibility form

Create an eligibility form, eligibility.html, with material design components.

- Of course use your app shell!
- Use material text fields with floating labels (mdc-floating-label)for:

  - `Age`
  - `Birth Date`
  - `Full Name`
  - `Username`
  - `Password`
  - `Confirm Password`

    - Make sure both those password inputs are `type="password"`! In fact, make all of them appropriate types (number | date | password | text)

- Two material checkboxes, with labels properly using the for attribute:

  - one #legal-checkbox with a label that includes the word "legal"
  - one #terms-checkbox with a label that includes the phrase "terms of service"

- Material buttons for the action
  - A Contained Button `Sign up`.
  - A Text button link at the bottom
    - contents `Info on Problem Gambling`
    - href points to `https://mnapg.org/understanding-problem-gambling/`
