# 03.1 Advanced CSS: Variables, Psuedo-classes, and Psuedo-elements

## Overview

## Goals and How We will Assess Student Learning

By the end of class, students will be able to:

- use psuedo-classes and psuedo-elements to style links, etc.
- use css custom properties

## Class Instructions

### Activity 0. Quiz over Readings

### Leverage Activity 1: Adding a color palette via css custom properties (variables), adding Links with psuedo-classes

- css variables
  - changing colors with varibles
- style links

### Blackjack ICP Activity 2: Add Custom Properties and Link Psuedo-classes

Use css custom properties and psuedo-classes to style your blackjack app:

- Add at least one css custom property to the root element: `--primary-color` which can be any color you'd like.
  - make sure you use it as a value somewhere as well!
- style your anchors, using the **link, visited, hover, focus, and active** states:
  - make sure your link style declarations are all there, and in the right order!
  - make text-decoration none on a:link, but underline on a:hover
    - we're testing this on the first anchor on your page, make sure the styles are applied to that link

### Leverage Activity 3: Pure CSS tooltips with the ::after psuedo-element (A11y warning)

- intro to psuedo-elements, like ::after
- tooltip creation, using :hover, ::after

### 4. Homework and Questions

For homework, add some pure css tooltips to display the value of the cards on the table.html when they are hovered, and tooltips for the definitions of the player's actions, as well as style the buttons.

- Lay table.html out nicely, if it is not already looking good.
- Add a tooltip to the cards when the ordered list's list items are hovered, with either ::before or ::after.
  - This rule should be in the table.css file, in the repo's root
  - Face cards must display "10", Aces "11/1", and the dealer's face-down card displays "?" when they are hovered. All other cards display their nominal value.
  - You may hardcode the values for now.
  - Use the custom html data attribute `data-blackjack-value`
  - Make at least one of the cards contain "Queen" of something, that's the one we test
- Reorganize the page, by moving the "Player's Options" definition list into the "Player's Actions" button area as tooltips
  - Add `data-user-hints` attribute, moving the definitions from the definition list to their appropriate button. Since html doesn't allow tags in data attributes, get rid of aside tags, but leave in the parentheses.
    - Make sure the "Hit" button includes the word "card"
  - restyle the buttons so they don't look like the netscape navigator nineties
  - enable all four buttons (Hit, Stand, Split, Insurance)
  - make sure the buttons display the data-user-hints on hover in a ::before or ::after psuedo-element
- we're only testing the desktop view, no need to design for mobile yet.
